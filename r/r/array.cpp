#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <cmath>
#include "lab.h"
enum gen { mono, down };
//using namespace std;

void swap(int* x, int* y)
{
	int temp = *x;
	*x = *y;
	*y = temp;

}

array::array(int size)
{
	mass = new int[size];
	len = size;
}

int array::bubble()
{
	int i = 0,
		j = 0,
		k = 0;
	for (i = 0; i < len; i++)
	{
		for (j = i; j < len - 1; j++)
		{
			if (mass[i] <= mass[j])
			{
				swap(&mass[i], &mass[j]);
			}
		}
	}
	return 0;
}

void array::printmass(ofstream& out)
{
	int i = 0;
	for (i = 0; i < len; i++)
	{
		out << mass[i] << " ";
	}
}

void array::genArray(int flag, int n, int size, ofstream& buff)
{
	buff.open("in.txt");
	if (flag == mono + 1)
	{
		for (int i = 0; i < size; i++){
			buff << n << " ";
		}

	}
	else
	{
		for (int i = 1; i <= size; i++){
			buff << i << " ";
		}

	}
	buff.close();
}