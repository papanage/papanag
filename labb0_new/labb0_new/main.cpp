#include <fstream>
#include <iterator>
#include <map>
#include <string>
#include <iostream>
#include <algorithm>
using namespace std;

/*class maper{
public:
	map<string, int> mu;
	void record(string s){
		mu[s]++;
	}
	void print(const pair<const string, int>& r){
		cout << r.first << ' ' << r.second << ' ';
	}
};*/
map<string, int> mu;

void record(string s){
	mu[s]++;
}
void print(const pair<const string, int>& r){
	cout << r.first << ' ' << r.second << ' ';
}
int main(int argc, char** argv){
	ifstream in("in.txt");
	istream_iterator<string> ii(in);
	istream_iterator<string> eos;
	//maper mymap;
	for_each(ii, eos, record);
	for_each(mu.rbegin(),mu.rend(), print);
	getchar();
	return 0;
}