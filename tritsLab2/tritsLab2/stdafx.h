/// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#ifndef TRIT_H
#define TRIT_H
#pragma once

#include "targetver.h"
typedef unsigned int uint;
#include <stdio.h>
#include <tchar.h>
#include <bitset>
#include <vector>

class TritSet
{
private:	uint prev_pos_w_p,
			prev_pos_no_w_p,
			flag;

public:
	const int const_int = 4 * sizeof(uint);//16
	uint* mas;
	uint reallen,
		realsize,
		len;
	TritSet& operator[](int pos);
	int operator=(uint logicvar);
	bool operator==(uint logicvar);
	uint GetTrit(uint pos);

	TritSet(uint size, uint s, uint f);
	uint capacity();
	void PrintOneInt(uint x);
	void PrintAllSet();
	uint NewSize(uint sizeel);
	void InitUn();



};

#endif

// TODO: reference additional headers your program requires here