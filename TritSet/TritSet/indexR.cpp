#include "tritR.h"
#include <iostream>
using namespace std;
enum trit{ Unknown, False, True };


TritSet::Index::Index(uint a, uint b, TritSet* c){
	whole_part = a;
	bit_part = b;
	myT = c;
}

uint TritSet::Index::operator=(uint logicvar)
{
	uint logv = 3;
	logv = logv << 2 * myT->const_uint - bit_part - 2;
	logv = ~logv;
	logicvar = logicvar << 2 * myT->const_uint - bit_part - 2;

	if (logicvar == Unknown){
		if (whole_part > myT->size_int - 1){

		}
		else {
			myT->mas[whole_part] &= logv;
			myT->mas[whole_part] |= logicvar;
		}
	}

	if (logicvar != Unknown){
		if (whole_part > myT->size_int - 1){
			myT->resize(whole_part + 1);

		}
			myT->mas[whole_part] &= logv;
			myT->mas[whole_part] |= logicvar;
	}
return 0;
}

bool TritSet::Index::operator ==(uint logicvar){
	if ((*this).GetTrit() == logicvar) return true;
	return false;
}

uint TritSet::Index::GetTrit(){
	if (whole_part > myT->size_int - 1) return Unknown;
	uint temp = myT->mas[whole_part] >> (myT->const_uint * 2 - bit_part - 2);
	temp &= 3;
}