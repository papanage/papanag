#include "tritR.h"
#include <iostream>
using namespace std;

enum  Trit{ Unknown, False, True };
namespace tables{
	uint table_and[3][3] = { { Unknown, False, Unknown }, { False, False, False }, { Unknown, False, True } };
	uint table_or[3][3] = { { Unknown, False, True }, { False, False, True }, { True, True, True } };
	uint table_not[3] = { Unknown, True, False };
};

TritSet::TritSet(uint size)
{
	//table_and[3][3] = { { Unknown, False, Unknown }, { False, False, False }, { Unknown, False, True } };
	//table_or[3][3] = { { Unknown, False, True }, { False, False, True }, { True, True, True } };
	//table_not[3] = { Unknown, True, False };
	mas = new uint[NewSize(size)];
	size_int = NewSize(size);
	f_z = NewSize(size);
	for (int i = 0; i < size_int; i++) mas[i] = Unknown;
}

TritSet::TritSet(const TritSet& my){
	f_z = my.f_z;
	uint* mask = new uint[my.size_int];
	for (int i = 0; i < my.size_int; i++) mask[i] = my.mas[i];
	mas = mask;
	size_int = my.size_int;
}

TritSet::~TritSet(){
	delete[] mas;
}


uint TritSet::NewSize(uint new_size){
	return new_size % const_uint  == 0 ? new_size / const_uint  : new_size / const_uint  + 1;
}


uint TritSet::capacity(){
	return size_int;
}


TritSet::Index TritSet::operator[](uint pos){
	Index posit(pos / const_uint, 2 * (pos - pos / const_uint * const_uint), this);
//	cout << "number int = " << pos / const_uint << " number of bit =  " << 2 * (pos - pos / const_uint * const_uint) << "\n";
	return posit;
}


uint TritSet::lastz(){
	uint i;
	for (i = capacity() - 1; i >= 0; i--){
		if (mas[i] != 0) {
			return i + 1;
		}
	}
	return f_z + 1;
}

uint TritSet::length(){
	uint temp = lastz();
	if (temp > size_int) return (temp - 1 )*const_uint;
	else {
		for (uint i = temp*const_uint - 1; i >= (temp - 1)*const_uint; i--){
			if ((*this)[i].GetTrit() != Unknown) return i+1;
		}
	}
}

void TritSet::shrink(){
	resize(lastz());
}

TritSet  TritSet::operator&(const TritSet b){
	return HelperForOper(0, tables::table_and, *this, b);
}

TritSet  TritSet::operator|(const TritSet b){
	return HelperForOper(1, tables::table_or, *this, b);
}


TritSet TritSet::operator~(){
	TritSet myNot(const_uint*size_int);
	for (uint i = 0; i < size_int*const_uint; i++){
		myNot[i] = tables::table_not[(*this)[i].GetTrit()];
	}
	myNot.f_z = f_z;
	return myNot;
}


uint TritSet::cardinality(uint value){
	uint count = 0;
	try{
		if (value < Unknown || value > True) throw  INCORECT_LOGIC_VALUE;
		if (value != Unknown){
			for (uint i = 0; i < lastz()*const_uint; i++){
				if ((*this)[i].GetTrit() == value) count++;
			}
		}
		else {
			uint last = lastz();
			if (last > size_int) return 0;
			uint j;
			for (j = last*const_uint; j > (last - 1)*const_uint; j--)
			if ((*this)[j].GetTrit()) break;
			for (uint i = 0; i <= j; i++) if ((*this)[i].GetTrit() == Unknown) count++;
		}
	}
	catch (int){
		return INCORECT_LOGIC_VALUE;
	}

	return count;
}

uint TritSet::operator[](uint pos) const{
	uint whole_part = pos % this->const_uint;
	uint bit_part = 2 * (pos - whole_part*this->const_uint);
	if (whole_part > this->size_int - 1) return Unknown;
	uint temp = this->mas[whole_part] >> (this->const_uint * 2 - bit_part - 2);
	temp &= 3;

}

TritSet TritSet::HelperForOper(uint flag, uint table[3][3], const TritSet a, const TritSet b){
	uint max_size = b.size_int;
	if (a.size_int > b.size_int) {
		max_size = a.size_int;
	}
	TritSet my_new(max_size*const_uint);
	for (uint i = 0; i < max_size*const_uint; i++){
			//my_new[i] = table[a[i].GetTrit()][b[i].GetTrit()];
		my_new[i] = table[a[i]][b[i]];
			//cout << a[i].GetTrit() << " " << b[i].GetTrit() << " " << table[a[i].GetTrit()][b[i].GetTrit()] << "\n";
	}
	return my_new;
}


void TritSet::resize(uint new_size){
	uint* newm = new uint[new_size];
	for (int i = 0; i < new_size; i++) newm[i] = Unknown;
	uint min;
	new_size < size_int ? min = new_size : min = size_int;
	memcpy(newm, mas, min*sizeof(uint));
	delete[]mas;
	mas = new uint[new_size];
	for (int i = 0; i < new_size; i++) mas[i] = newm[i];
	size_int = new_size;
	return;
}

TritSet& TritSet::operator=(const TritSet& b){
	if (this != &b) {
		f_z = b.f_z;
		size_int = b.size_int;
		delete[] mas;
		mas = new uint[size_int];
		for (int i = 0; i < size_int; i++) {
			mas[i] = b.mas[i];
		}
	}
	return *this;
}

void TritSet::trim(uint lastIndex){
	for (uint i = lastIndex; i < lastz()*const_uint; i++){
		(*this)[i] = Unknown;
		//shrink()
		//(*this).size_int = lastIndex / const_uint + 1;
	}

}



void TritSet::PrintOneInt(uint x)
{
	uint temp = 0;
	for (int i = 0; i < const_uint * 2; i++)
	{
		temp = x >> (const_uint * 2 - i - 1);
		cout << (temp & (uint)1);
	}
	cout << "\n";
}

void TritSet::PrintAllSet()
{
	for (int i = 0; i < size_int; i++)
	{
		PrintOneInt(mas[i]);
	}
	cout << "last = " << lastz() << "\n";
	cout << "capacity = " << capacity() << "\n";
	cout << "count true = " << cardinality(True) << "\n";
	cout << "count false = " << cardinality(False) << "\n";
	cout << "count Un = " << cardinality(Unknown) << "\n";
	cout << "f_z = " << f_z << "\n";
	cout << "length = " << length() << "\n";
}