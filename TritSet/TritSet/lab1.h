#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
typedef unsigned int uint;
//enum  trit{ Unknown, False, True };




class TritSet;




class TritSet{
	uint size_int;
	const uint const_uint = sizeof(uint)* 4;
	uint* mas;
	class Index{
		uint whole_part,
		bit_part;
		TritSet* myT;
	public:
		Index(uint a, uint b, TritSet* c);
		uint operator=(uint logicvar);
		uint GetTrit();
	};

public:
	uint f_z;
	// ����� ��� []
	TritSet(uint size);				//����������� �������
	TritSet(const TritSet& my);		//����������� �����������
	~TritSet();
	uint NewSize(uint new_size);	//��������� ����������� ����� � ������ ��� ���-�� ���������
	void PrintOneInt(uint x);
	void PrintAllSet();
	uint capacity();
	Index operator[](uint pos);
	void resize(uint new_size);
	void shrink();
	uint cardinality(uint value);
	TritSet operator&(TritSet b);
	TritSet operator|(TritSet b);
	TritSet operator~();
	uint lastz();
	uint length();
	void trim(uint lastIndex);
	TritSet& operator=(const TritSet& b);
	void InitUn();
	TritSet HelperForOper(uint flag, uint table[3][3], TritSet a, TritSet b);
};


