
#pragma once
#define INCORECT_LOGIC_VALUE (-101);
#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
typedef unsigned int uint;
//enum  Trit2{ Unknown, False, True};

class TritSet{
//	const uint table_and3[3][3] = { { 0, 1, 0 }, { 1, 1, 1 }, { 0, 1, 2 } };
	//uint table_or[3][3] = { { Unknown, False, True }, { False, False, True }, { True, True, True } };
	//uint table_not[3] = { Unknown, True, False };

	uint size_int;
	uint f_z;
	const uint const_uint = sizeof(uint)* 4;
	uint* mas;
class Index{
		uint whole_part,
		bit_part;
		TritSet* myT;
		
	public:
		Index(uint a, uint b, TritSet* c);
		uint operator=(uint logicvar);
		uint GetTrit();
		bool TritSet::Index::operator ==(uint logicvar);
		};

uint lastz();
void resize(uint new_size);
uint NewSize(uint new_size);
TritSet HelperForOper(uint flag, uint table[3][3], const TritSet a, const TritSet b);
public:
	
	TritSet(uint size);				//конструктор базовый
	TritSet(const TritSet& my);		//конструктор копирования
	~TritSet();
	void PrintOneInt(uint x);
	void PrintAllSet();
	uint capacity();
	Index operator[](uint pos);
	void shrink();
	uint cardinality(uint value);
	TritSet operator&(const TritSet b);
	TritSet operator|(const TritSet b);
	TritSet operator~();
	uint operator[](uint pos) const;
	uint length();
	void trim(uint lastIndex);
	TritSet& operator=(const TritSet& b);
	
};

