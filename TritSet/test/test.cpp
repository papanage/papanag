// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "tritR.h"
#include "gtest/gtest.h"
enum trit{ Unknown, False, True };
class CRectTest : public ::testing::Test {
};


TEST_F(CRectTest, CheckPerimeter)
{
	TritSet set(1000);
	// length of internal array
	size_t allocLength = set.capacity();
	assert(allocLength >= 1000 * 2 / 8 / sizeof(uint));
	// 1000*2 - min bits count
	// 1000*2 / 8 - min bytes count
	// 1000*2 / 8 / sizeof(uint) - min uint[] size

	//�� �������� ������� ������
	set[1000000000] = Unknown;
	assert(allocLength == set.capacity());

	// false, but no exception or memory allocation
	if (set[2000000000] == True){}
	assert(allocLength == set.capacity());

	//��������� ������
	set[1000000000] = True;
	assert(allocLength < set.capacity());

	//no memory operations
	allocLength = set.capacity();
	set[1000000000] = Unknown;
	set[1000000] = False;
	assert(allocLength == set.capacity());

	//������������ ������ �� ���������� �������� ��� 
	//�� �������� ������������ ��� �������� ���������� �������������� �����
	//� ������ ������ ��� ����� 1000�000
	set.shrink();
	assert(allocLength > set.capacity());

}

TEST_F (CRectTest, My){
	TritSet set(1000);
	for (uint i = 0; i < 1000; i++){
		if (i % 2 == 0) set[i] = True;
		else set[i] = False;
	}
	TritSet set3(30);
	set3 = set3 & set;
	ASSERT_TRUE(set3.capacity() == set.capacity());
	ASSERT_TRUE(set.cardinality(True) == set.cardinality(False));
	TritSet set2(1000);
	set2 = ~set;
	set2 = set & set2;
	//ASSERT_TRUE(1000 == set2.cardinality(False));
	set2.trim(4);
	set2.shrink();
	ASSERT_TRUE(set2.capacity() == 1);
	ASSERT_TRUE(-101 == set2.cardinality(5));
	ASSERT_TRUE(set2.capacity() == 1);
}
