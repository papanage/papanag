
#define FIRST_PLAYER 1
#define SECOND_PLAYER 2 

class player{
	field* f;
	string name;
	int number;
	game_rules gr;
	friend class arbitr;
public:
	player(const game_rules gr, string type, uint num){
		if (type == "player"){
			cout << "Put your name " << num << " player: ";
			cin >> name;
			number = num;
			system("cls");
			f = new field(gr.map_size, 1);
		}
	}
	void WantToPlaceShips(arbitr& this_arbitr){
		f->draw(name, gr.c[0], 1);
		this_arbitr.access_ships(f, name);
	}
	void WantToPlaceShips(arbitr& this_arbitr, string type){
		if (type == "player") WantToPlaceShips(this_arbitr);
		this_arbitr.access_ships(f, name, this_arbitr.get_total(number), this_arbitr.get_type());
	}
	int WantToShot(arbitr& this_arbitr, player* pl){
		string coord;
		cout << "\n					"  << name <<  " want shot:\n";
		cin >> coord;
		return this_arbitr.access_shot(coord, pl->f, number, name);
	}
	void draw(arbitr& this_arbitr, bool is_my_turn){
		f->draw(name, this_arbitr.get_total(number), is_my_turn);
	}
};


class game{

	player* pl1;
	player* pl2;
	friend class arbitr;
	int winner;	
	void draw_all(arbitr& this_arbitr, uint mod){
		system("cls");
		if (mod == 1){
			pl1->draw(this_arbitr, true);
			pl2->draw(this_arbitr, false);
		}
		if (mod == 2){
			pl1->draw(this_arbitr, false);
			pl2->draw(this_arbitr, true);
			
		}
	}
public:	
	game(arbitr& this_arbitr){
		pl1 = new player(this_arbitr.gr, "player", FIRST_PLAYER);
		pl2 = new player(this_arbitr.gr, this_arbitr.get_type(), SECOND_PLAYER);
		winner = -1;
	}
	int get_winner(){
		if (winner <= 0) throw(-101);
		return winner;
	}

	void run(arbitr& this_arbitr){
		pl1->WantToPlaceShips(this_arbitr, "player");
		system("cls");
		pl2->WantToPlaceShips(this_arbitr, this_arbitr.get_type());
		
		while (this_arbitr.get_total(FIRST_PLAYER) && this_arbitr.get_total(SECOND_PLAYER)){
			draw_all(this_arbitr, FIRST_PLAYER);//1 - 1 ��� 1 �������. 2 - ��� �������, 3- ����1 ����
			while (int u = pl1->WantToShot(this_arbitr, pl2)){
				draw_all(this_arbitr, FIRST_PLAYER);
				winner = u;
				if (winner > 0) break;
			}
			if (winner > 0) break;
			draw_all(this_arbitr, SECOND_PLAYER);
			while (int u = pl2->WantToShot(this_arbitr, pl1)){
				draw_all(this_arbitr, SECOND_PLAYER);
				winner = u;	
				if (winner > 0) break;
			}
			
		}
		draw_all(this_arbitr, 2);
	}
};



