class field;
class pointer{
	game_rules gr;
	uint x;
	uint y;
	uint beh;
	friend class field;
	friend class arbitr;
	pointer(){
		x = 0;
		y = 0;
		beh = gr.empty;
	}
	pointer(uint x_pos, uint y_pos){
		x = x_pos;
		y = y_pos;
		beh = gr.empty;
	}
public:	
	uint Get_x(){
		return x;
	}
	uint Get_y(){
		return y;
	}
	char Get_beh(){
		return beh;
	}
};