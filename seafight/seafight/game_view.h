class game_view{
public:
	void show_total(string name, int total){
		cout << "            player: " << name << " total = " << total;
	}
	void show_turn(bool is_turn){
		if (is_turn) cout << "				YOUR TURN";
	}
	void take_coord(uint i, uint j, string& coord){
		cout << "\n\nWrite start coord for " << j << " " << i << "-place ship (example a5- , d7|; '-' - goriz, '|' - vert\n ";
		cin >> coord;
	}
	void show_message(string from, string message){
		cout << from << ": " << message;
	}
	void clear(){
		system("cls");
	}
};