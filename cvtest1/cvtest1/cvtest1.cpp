// cvtest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <C:/opencv/build/include/opencv2/opencv.hpp>
#include <iostream>
#include <ctime>
using namespace cv;
using namespace std;
int _tmain(int argc, _TCHAR* argv[])
{
	CvCapture *capture = cvCreateCameraCapture(0);
	if (!capture) return 0;
	double start1 = clock();
	double start2;
	double finish2;
	while (1) {
		start2 = clock();
		IplImage *frame = cvQueryFrame(capture);
		if (!frame) break;

		cvShowImage("test", frame);

		finish2 = clock();
		/*IplImage *image = cvCloneImage(frame);
		cvSmooth(frame, image, CV_BLUR, 3, 3);
		for (int y = 0; y<image->height; y++) {
		uchar *ptr = (uchar*)(image->imageData +
		y*image->widthStep);
		for (int x = 0; x<image->width; x++) {
		ptr[3 * x] = 0; // Blue
		ptr[3 * x + 2] = 0; // Red
		}
		}		*/
		//cvShowImage("smooth", image);
		char c = cvWaitKey(33);
		if (c == 27) break;
	}
	double finish1 = clock();
	//cout << (finish1 - start1) / (finish2 - start2) << "\n";
	getchar();
	cvReleaseCapture(&capture);
	cvDestroyWindow("test");

	return 0;
}
