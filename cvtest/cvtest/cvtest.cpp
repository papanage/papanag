// cvtest.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <opencv2/opencv.hpp>
#include <iostream>
#include <ctime>
using namespace cv;
using namespace std;
int main(int argc, char *argv[])
{
	CvCapture *capture = cvCreateCameraCapture(0);
	if (!capture) return 0;
	
	
	int i = 1;
	 int k = 1000;
	double s1 = clock();
	for (k = 15; k < 50; k++){
		i = 1;
		double s2 = 0,
			   f2 = 0,
			   s1 = 0,
			   f1 = 0,
			   s3 = 0,
			   f3 = 0,
			   s4 = 0,
			   f4 = 0,
			   res = 0;
		while (i) {
			s1 += clock();
				s3 += clock();
					IplImage *frame = cvQueryFrame(capture);
					if (!frame) 	break;
					IplImage *image = cvCloneImage(frame);
					cvSmooth(frame, image, CV_BLUR, 3, 3);
					
					for (int y = 0; y<image->height; y++) {
						uchar *ptr = (uchar*)(image->imageData +
							y*image->widthStep);
						for (int x = 0; x<image->width; x++) {
							if (!(y % 25) || !(x % 25)){
								ptr[3 * x + 1] = 0;
								ptr[3 * x + 2] = 0;
								ptr[3 * x] = 0;
							}
						}
					}					cvShowImage("smooth", image);
				f3 += clock();
				s4 += clock();
					cvShowImage("test", frame);
				f4 += clock();
				s2 = s2 + clock();
					char c = cvWaitKey(k);
				f2 = f2 + clock();
			f1 += clock();
			if (i ==  2) break;
			i++;
			
		}

		res = f1 - s1;
		cout << "was: " << 1000 / ((f1 - s1) / i) << " expected: " << 1000 / k << " "; //���������� �������� ��� � ���������������
		cout << "proc waiting = " << 100 * ((f2 - s2)) / res << "%; ";
		cout << "proc obrabotka = " << 100 * ((f3 - s3)) / res << "%; ";
		cout << "proc output  = " << 100 * ((f4 - s4)) / res << "%" << "\n";
	}
	cvReleaseCapture(&capture);
		cvDestroyWindow("test");
	getchar();
}

