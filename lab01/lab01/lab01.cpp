



#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <map>
#include <string>

#define NOFILE -101
typedef std::map<std::string, int>  mapT;
using namespace std;

bool CheckLetter(char a)
{
	if ((a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z') || (a >= '0' && a <= '9')) return true;
	return false;
}

bool IsWord(string a)
{
	if (!a.length()) return false;
	return true;
}


class CVS
{
public:
	map <string, int> tableCSV;
	unsigned int countAll,
		countDifword;
	CVS()
	{
		countAll = 0;
	}

	void PrintCVS(ofstream& out)
	{
		unsigned int len = tableCSV.size();
		auto i = tableCSV.begin();
		auto max = i;													//���� �� ������ �� ���������� ��������� �� 
		for (int j = 0; j < len; j++)
		{
			max = tableCSV.begin();									//��������� � ������� ����, �������� - ������� � � ���������
			for (i = tableCSV.begin(); i != tableCSV.end(); ++i)
			{

				if (i->second > max->second)
				{
					max = i;
				}

			}
			out << max->first << ", " << max->second << ", " << (float)max->second / countAll * 100 << "\n";
			tableCSV.erase(max);
		}
	}


	void ComputeWord(string buf, int* count)
	{
		if (IsWord(buf))						//���� ���������� 2 � ������ ����������� ������, �� last - first = 0, ����� ��������� �� ���
		{
			(*count)++;
			tableCSV[buf]++;
		}
	}

	void ComputeString(string a)
	{
		//����� ������������� ���������, ������, ������ �������. 
		//��������� �� ��������� � �������� �������� (���-��� ��������) ������������ ������
		int	first = 0,								//1 index
			countOfWord = 0,						// ����������, ����� ������� ������� ���� � ����� ��������� ������ - ����� ��� ����������
			last = 0;								//2 index
		string buf;									//�����, �������� �����

		while (last < a.length())					//���� �� ����� ������ �����������
		{
			while (CheckLetter(a[last])) last++;	//���� ����������� �� ����������� - �������, ��� ����������� >= 1 ������		
			buf = a.substr(first, last - first);	//�������� ���� ��������� - ������������� �����
			last++;
			first = last;							//�������� 1 ������ �� ��������� ����� ����������

			ComputeWord(buf, &countOfWord);
		}
		countAll += countOfWord;
	}
};


class ReaderandWriter
{
public:
	ifstream in;
	ofstream out;
	string word;

	ReaderandWriter(char* ifilename, char* ofilename)
	{
		in.open(ifilename);
		out.open(ofilename);
		if (!in) exit(NOFILE);
		if (!out) exit(NOFILE);
	}

	~ReaderandWriter()
	{
		in.close();
		out.close();
	}

	void ReadFile(CVS& myCVS)
	{
		while (getline(in, word))
		{
			myCVS.ComputeString(word);
		}
	}

};
//fsdfsd
int _tmain(int argc, _TCHAR* argv[])
{
	ReaderandWriter myRW("input.txt", "output.txt");
	CVS myCVS;
	myRW.ReadFile(myCVS);
	myCVS.PrintCVS(myRW.out);
	int y = 0;
	return 0;
}

