
#include <iostream>
#include <fstream>
#include <string>
#include <time.h> 
#include <stdio.h>
using namespace std;

enum gen { mono, down };


void swap(int* x, int* y)
{
	int temp = *x;
	*x = *y;
	*y = temp;

}

class array{
public:
	int* mass;
	int len;
	array(int size);
	int bubble();
	void printmass(ofstream& out);
	void genArray(int flag, int n, int size, ofstream& out);
};


class RW
{
public:
	const int NOFILE = -101;
	ofstream buff;
	ifstream in;
	ofstream out;
	RW()
	{
		char* buf = new char[30];
		//cout << "FILE for input:\n ";
		//cin >> buf;
		//in.open(buf);
		//if (!in.is_open())
			//exit(NOFILE);
		cout << "FILE for output:\n ";
		cin >> buf;
		out.open(buf);
		if (!out.is_open())
			exit(NOFILE);
		delete(buf);
	}

	void getArray(array myArray)
	{
		in.open("in.txt");
		int i = 0;
		for (i = 0; i < myArray.len; i++)
		{
			in >> myArray.mass[i];
		}
		in.close();
	}

	void StartTest()
	{
		struct timespec start, end;
		int buf;
		int n;
		int count;
		cout << "Choose variant for testArray ('mono' 1 - v or 'down' 2 - v)\n ";
		cin >> buf;
		switch (buf)
		{
			case (mono + 1) :
			{

								cout << "Write number for monoArray\n ";
								cin >> n;
								cout << "Write count of " << n << "\n ";
								cin >> count;
								array myArray(count);
								myArray.genArray(1, n, count, buff);
								getArray(myArray);
								clock_gettime(CLOCK_MONOTONIC_RAW, &start);
								myArray.bubble();
								clock_gettime(CLOCK_MONOTONIC_RAW, &end);
								myArray.printmass(out);
								break;

			}
			case (down + 1) :
			{
								cout << "Write count of numbers" << "\n ";
								cin >> count;
								array myArray(count);
								myArray.genArray(2, 0, count, buff);
								getArray(myArray);

								clock_gettime(CLOCK_MONOTONIC_RAW, &start);
								myArray.bubble();
								clock_gettime(CLOCK_MONOTONIC_RAW, &end);
								myArray.printmass(out);
								break;

			}
		}
		printf("Time taken: %lf sec.\n", end.tv_sec - start.tv_sec + 0.000000001*(end.tv_nsec - start.tv_nsec));
		
	}
};


using namespace std;

int main(int argc, char* argv[])
{
	RW myRW;
	myRW.StartTest();
	//struct timespec start, end;
	
	
	//clock_gettime(CLOCK_MONOTONIC_RAW, &start);
	
	//clock_gettime(CLOCK_MONOTONIC_RAW, &end);
	//printf("Time taken: %lf sec.\n",  end.tv_sec - start.tv_sec + 0.000000001*(end.tv_nsec - start.tv_nsec));
	
	//getchar();
	return 0;
}



array::array(int size)
{
	mass = new int[size];
	len = size;
}

int array::bubble()
{
	int i = 0,
		j = 0;
	for (i = 0; i < len; i++)
	{
		for (j = i; j < len; j++)
		{
			if (mass[i] < mass[j]) swap(&mass[i], &mass[j]);
		}
	}
	return 0;
}

void array::printmass(ofstream& out)
{
	int i = 0;
	for (i = 0; i < len; i++)
	{
		out << mass[i] << " ";
	}
}

void array::genArray(int flag, int n, int size, ofstream& buff)
{
	buff.open("in.txt");
	if (flag == mono + 1)
	{
		for (int i = 0; i < size; i++){
			buff << n << " ";
		}
		
	}
	else
	{
		for (int i = 1; i <= size; i++){
			buff << i << " ";
		}

	}
	buff.close();
}