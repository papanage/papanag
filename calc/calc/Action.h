using namespace std;
class Action{

operation_archieve		base;
stack<double>			stack;
map <string, double>	def;
ifstream				in;
ofstream				out;

public:


Action(int argc, char** argv){
	if (argc > 1) {
		in.open(argv[1]);
		if (!in.is_open()) exit(600);
		cin.rdbuf(in.rdbuf());
	}
}

~Action(){
	if (in.is_open()) in.close();
}

string invided(string& s, char delim){
		size_t first_gape = s.find_first_of(delim);
		if (first_gape < s.length()){
			string command = s.substr(0, first_gape);
			s.erase(s.begin(), s.begin() + first_gape + 1);
			return command;
		}
		else {
			string command = s;
			s.clear();
			return command;
		}
}

int compute_command_string(string command_prom){
	Context cont(stack, def);
	string command;
	while (command_prom.length() != 0){
		string command = invided(command_prom, ' ');
		cont.parametres.push_back(command);
	}

	base.get_creator(cont.parametres[0])->act(cont);
	stack = cont.st;//boost.Ref
	def = cont.def;
	return 0;
}

int allrun(){
		while (cin){
			string comand_prom;
			getline(cin, comand_prom, '\n');
			if (comand_prom.length() != 0) compute_command_string(comand_prom);
			}
		return 0;
	}
double top(){
	if (!stack.empty()) return stack.top();
}
};

