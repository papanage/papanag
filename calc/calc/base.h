class operation_archieve{
	const int count_oper = 10;
	vector<Creator*> C_Operations;
	vector<string> names;
	map<string, Operation*> alf;
	exeptionBADOPER bo;

public:
	operation_archieve(){
		C_Operations.push_back(new CreatorPUSH);
		names.push_back("PUSH");
		C_Operations.push_back(new CreatorPOP);
		names.push_back("POP");
		C_Operations.push_back(new CreatorDEFINE);
		names.push_back("DEFINE");
		C_Operations.push_back(new CreatorSUM);
		names.push_back("+");
		C_Operations.push_back(new CreatorSUB);
		names.push_back("-");
		C_Operations.push_back(new CreatorPRO);
		names.push_back("*");
		C_Operations.push_back(new CreatorDIV);
		names.push_back("/");
		C_Operations.push_back(new CreatorSQRT);
		names.push_back("SQRT");
		C_Operations.push_back(new CreatorPRINT);
		names.push_back("PRINT");
		C_Operations.push_back(new CreatorCOM);
		names.push_back("#");
		
		for (int i = 0; i < count_oper; i++){
			alf.insert(pair<string, Operation*>(names[i], C_Operations[i]->FactoryOperation()));
		}
	}

	Operation* get_creator(string s){
		
			if (alf.find(s) != alf.end())
			return alf[s];
			throw(UNKNOWN_OPERATION);
	
	}


};