
class Operation{
public:
	virtual int act(Context& a) = 0;
	int type;//
};

class OperationPUSH : public Operation{
public:
	OperationPUSH(){ type = 2; }
	int act(Context& a){
			//cout << "push" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.def.find(a.parametres[1]) != a.def.end()) a.st.push(a.def[a.parametres[1]]);
			else{
				char* pEnd;
				double temp = strtod(a.parametres[1].c_str(), &pEnd);
				if (*pEnd) throw(BAD_TYPE_PARAMETRES);
				if (temp == numeric_limits<double>::infinity()) throw(BAD_SIZE_PARAMETRES);
				a.st.push(temp);
				
			
		}

		return 0;
	}
};

class OperationPOP: public Operation{
public:
	OperationPOP(){ type = 1;}
	int act(Context& a){
		//cout << "pop" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			a.st.pop();
			return 0;
	}
};

class OperationDEFINE : public Operation{
public:
	OperationDEFINE(){ type = 3; }

	int act(Context& a){
			char* pEnd;
			//cout << "define" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.def.find(a.parametres[1]) != a.def.end()) throw(MAP_MULTI_DEFENITION);
			double temp = strtod(a.parametres[2].c_str(), &pEnd);
			if (*pEnd) throw(BAD_TYPE_PARAMETRES);
			a.def.insert(pair<string, double>(a.parametres[1], atof(a.parametres[2].c_str())));
		return 0;
	}
};

class OperationSUM : public Operation{
public:
	OperationSUM(){ type = 1; }
	int act(Context& a){
			//cout << "sum" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			double res = a.st.top();
			a.st.pop();	
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			res += a.st.top();
			a.st.pop();
			if (res == numeric_limits<double>::infinity()) throw (BAD_SIZE_PARAMETRES);
			a.st.push(res);
		
		return 0;
	}
};

class OperationSUB : public Operation{
public:
	OperationSUB(){ type = 1; }
	int act(Context& a){
	
			//cout << "min" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			double res = a.st.top();
			a.st.pop();
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			res -= a.st.top();
			a.st.pop();
			if (res == numeric_limits<double>::infinity()) throw (BAD_SIZE_PARAMETRES);
			a.st.push(res);
	
		return 0;
	}
};

class OperationPRO : public Operation{
public:
	OperationPRO(){ type = 1; }
	int act(Context& a){
			//cout << "pro" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			double res = a.st.top();
			a.st.pop();
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			res *= a.st.top();
			a.st.pop();
			if (res == numeric_limits<double>::infinity()) throw (BAD_SIZE_PARAMETRES);
			a.st.push(res);
		return 0;
	}
};

class OperationDIV : public Operation{
public:
	OperationDIV(){ type = 1; }
	int act(Context& a){
	
		//	cout << "div" << "\n";
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			double res = a.st.top();
			a.st.pop();
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			if (a.st.top() == 0) throw (DIVION_BY_ZERO);
			res /= a.st.top();
			a.st.pop();
			if (res == numeric_limits<double>::infinity()) throw (BAD_SIZE_PARAMETRES);
			a.st.push(res);

		return 0;
	}
};

class OperationSQRT : public Operation{
public:
	OperationSQRT(){ type = 1; }
	int act(Context& a){
		//cout << "sqrt" << "\n";
		
			if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
			if (a.st.empty()) throw (STACK_IS_EMPTY);
			if (a.st.top() < 0) throw (SQRT_FROM_MINUS);
			int res = sqrt(a.st.top());
			a.st.pop();
			a.st.push(res);

		return 0;
	}
};

class OperationPRINT : public Operation{
public:
	OperationPRINT(){ type = 1; }
	int act(Context& a){
		//cout << "print" << "\n";
		if (a.parametres.size() != type) throw(BAD_COUNT_PARAMETRES);
		if (a.st.empty()) throw (STACK_IS_EMPTY);
		cout << a.st.top() << "\n";
		return 0;
	}
};

class OperationCOM : public Operation{
public:
	int act(Context& a){
		return 0;
	}
};
