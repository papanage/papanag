using namespace std;
#include "fenv.h"
#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <stack>
#include <string>
#include <stdio.h>
#include <algorithm>
#include <cstdlib>
#include "exeptions.h"
#include "Context.h"
#include "Operation.h"
#include "Creator.h"
#include "base.h"
#include "Action.h"


int main(int argc, char**argv){
	
	Action f(argc, argv);
	while (1){
		try{
			f.allrun();
			break;
		}
		catch (exeption n){
			cout << n.code_s <<"\n";
		}
	}
	getchar();
	return 0;
}