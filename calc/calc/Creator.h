//#include "Operation.h"


class Creator{
public:
	virtual Operation* FactoryOperation() = 0;
};

class CreatorPUSH : public Creator{
public:
	Operation* FactoryOperation(){ 
		return new OperationPUSH; 
	}
};

class CreatorPOP : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationPOP;
	}
};

class CreatorDEFINE : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationDEFINE;
	}
};

class CreatorSUM : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationSUM;
	}
};

class CreatorSUB : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationSUB;
	}
};

class CreatorPRO : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationPRO;
	}
};

class CreatorDIV : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationDIV;
	}
};

class CreatorSQRT : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationSQRT;
	}
};


class CreatorPRINT : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationPRINT;
	}
};

class CreatorCOM : public Creator{
public:
	Operation* FactoryOperation(){
		return new OperationCOM;
	}
};
